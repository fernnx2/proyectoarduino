#include <Servo.h>
#include <Sensor.h>
#include <Pulsador.h>
#include <Display.h>
#include <Buzzer.h>
#include <GFButton.h>


byte pinesDisplay[] ={2,3,4,5,6,7,8};
const byte pinBuzzer = 13;
const byte pinServo = 9;
const byte pinSensor = A0;
const byte pinPulsador =12;
unsigned long previousMillis =0;
const long interval = 500;



GFButton pulsador(pinPulsador);
Buzzer buzzer(pinBuzzer);
Display display(pinesDisplay);
Sensor sensor(pinSensor);
Servo servo;
bool estado =false;
bool estado_anterior = false;
void setup() {
  Serial.begin(9600);
  Serial.println(F("-------Iniciando Sistema de Protección Anti-Encendios---------"));
  servo.attach(pinServo);
  Serial.println(F("-----Obteniendo Datos el Sensor de Humo-----"));
  //attachInterrupt(digitalPinToInterrupt(pinPulsador), test, RISING);
 
}

void loop() {

    
    delay(1000);
    int valor = sensor.valorAnalogo();
    int valorDecimal = sensor.valorDecimal(valor);
    if(valorDecimal>0 && valorDecimal<5){
      Serial.println("Valores  de Temperatura y Humo en parametros Normales");
      }
    if(valorDecimal>4 && valorDecimal<7){
      Serial.println("Valores  de Temperatura y Humo WARNING!!!!....");
      buzzer.beep(700);
      display.encenderDisplay(valorDecimal);
      }
    if(valorDecimal > 6 && valorDecimal<8){
      Serial.println("Valores  de Temperatura y Humo WARNING!!!!....");
      display.encenderDisplay(valorDecimal);
      for(int x =0; x<4;x++){
        buzzer.beep(400);
        }
      
      }
      
    if(valorDecimal>7){
      Serial.println("Valores  de Temperatura y Humo críticos DANGER!!!!....");
      Serial.println("Accionando dispositivo Anti-Encendios!");
      display.encenderDisplay(valorDecimal);
      for(int x =0; x<6;x++){
        buzzer.beep(700);
        }
        servo.write(180);
      }  
    Serial.print("Valor Decimal:  ");
    Serial.print(valorDecimal);
    Serial.println(" ");
    
    if(estado){
          test();
          
    }
    unsigned long currentMillis = millis();

    if(currentMillis  - previousMillis >= interval){
      previousMillis = currentMillis;
      estado = pulsador.isPressed();
        
      }
 
}


void test(){
    Serial.println("---Test---");
    delay(1000);
    buzzer.beep(500);
    delay(1000);
    servo.write(0);
    display.apagarDisplay();
    display.encenderDisplay(3);
    delay(1000);
    servo.write(180);
    delay(1000);
    display.apagarDisplay();
    servo.write(0);
  
  }
